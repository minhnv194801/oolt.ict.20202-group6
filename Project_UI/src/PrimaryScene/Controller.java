package PrimaryScene;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Controller {
	
	@FXML
	Button btnScene1;
	
//------------------Methods------------------
	
	public void handleBtn1() throws Exception{
		
		Parent root = FXMLLoader.load(getClass().getResource("MainScene.fxml"));
		
		Stage window = (Stage) btnScene1.getScene().getWindow();
		window.setScene(new Scene(root, 750, 500));
		
	}
	
}
