package PrimaryScene;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

//import java.awt.event.ActionEvent;
//import java.net.URL;
//import java.util.ResourceBundle;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.scene.control.ComboBox;
//import javafx.scene.control.Label;

public class MainSceneController {


	//-----------------ComboBox Phiên GD-----------------
//    @FXML
//    public ComboBox<String> comboBox1;
//
//    @FXML
//    public Label label1;
//
//    ObservableList<String> list1 = FXCollections.observableArrayList("Sáng", "Chiều");
//    public void initialize1(URL location, ResourceBundle resources) {
//        comboBox1.setItems(list1);
//    }
//    public void comboBoxChanged1 (ActionEvent event){
//        label1.setText(comboBox1.getValue());
//    }
    

    //------------ComboBox Sàn CK-------------------------------------
//   @FXML
//   public ComboBox<String> comboBox2;
//
//   @FXML
//   public Label label2;
//
//   ObservableList<String> list2 = FXCollections.observableArrayList("HOSE", "HNX", "UPCOM");
//   public void initialize2(URL location, ResourceBundle resources) {
//       comboBox2.setItems(list2);
//   }
//   public void comboBoxChanged2 (ActionEvent event){
//       label2.setText(comboBox2.getValue());
//   }

   //	---------------Button Back to Primary Scene------------------
	@FXML
	Button btnScene2;
	
	
	public void handleBtn2() throws Exception{
		
		Parent root = FXMLLoader.load(getClass().getResource("FirstScene.fxml"));
		
		Stage window = (Stage) btnScene2.getScene().getWindow();
		window.setScene(new Scene(root, 750, 500));
		
	}
}
