package hust.soict.globalict.main.initializer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import hust.soict.globalict.main.data.MyData;

/**
 * Initializer is used to create MyData with the data extracted from articles
 */
public class Initializer {

	public Initializer() {
		
	}

	public static void initialData(List<MyData> myData) {
		String folder = "articles/";
		String fileList[] = {"Vnexpress.Articles.csv", "TNCK.Articles.csv", "Cafef.Articles.csv"};

		try {
			for (String file: fileList) {
				BufferedReader br = new BufferedReader(new FileReader(folder + file));
				String line;
				line = br.readLine();
				while ((line = br.readLine()) != null) {
					MyData tmpData = new MyData(line);
					if (tmpData.getId() != 0) {
						myData.add(tmpData);
					}
				}
				br.close();
			}
		} catch (IOException e) {
			System.out.println("ERROR: File not found");
		}
	}

}
