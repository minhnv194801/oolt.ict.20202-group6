package hust.soict.globalict.main.tag;
import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.main.data.MyData;

/**
 * Class Tag is an abstract class that will collect relevant data to the tag from data list
 * It can print out the data it had collected
 */
public abstract class Tag implements Mergeable<Tag> {

	protected List<String> relevantData = new ArrayList<String>();

	public Tag() {

	}

	/**
	 * This method will pick data stored in dataList for data relevant to the tag
	 */
	public abstract void collectData(List<MyData> myData);

	public void merge(Tag... tagsToMerged) {
		List<String> mergedData = new ArrayList<String>();
		for (String data: relevantData) {
			boolean isShare = true;
			for (Tag anotherTag: tagsToMerged) {
				if (!anotherTag.contain(data)) {
					isShare = false;
					break;
				}
			}
			if (isShare) {
				mergedData.add(data);
			}
		}
		relevantData = mergedData;
	}

	public void print() {
			for (String data: relevantData) {
				System.out.println(data);
			}

	}

	public void addData(String... newDataList) {
		for (String newData: newDataList) {
			relevantData.add(newData);
		}
	}

	public boolean contain(String data) {
		return relevantData.contains(data);
	}

	public boolean isEmpty() {
		return relevantData.isEmpty();
	}
}