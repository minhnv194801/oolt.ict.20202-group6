package hust.soict.globalict.main.tag;
import java.util.List;

import hust.soict.globalict.main.data.MyData;

/**
 * DateTag inherit the class Tag
 * it is the kind of Tag that collect data based on the date of the article
 * WARNING: DateTag is only work for date written in the format of d/m/y
 */
public class DateTag extends Tag {

	private int day, month, year;

	public DateTag(String date) {
		day = Integer.parseInt(date.split("/")[0]);
		month = Integer.parseInt(date.split("/")[1]);
		year = Integer.parseInt(date.split("/")[2]);
	}

	public void collectData(List<MyData> dataList) {
		for (MyData data: dataList) {
			if (day == data.getDay() && month == data.getMonth() && year == data.getYear()) {
				List<String> contentList = data.getContents();
				for (String content: contentList) {
					this.addData(content);
				}
			}
		}
	}

}