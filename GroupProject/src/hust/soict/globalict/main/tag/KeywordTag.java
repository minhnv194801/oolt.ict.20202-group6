package hust.soict.globalict.main.tag;
import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.main.data.MyData;

/**
 * KeywordTag inherit the class Tag
 * it is the kind of Tag that collect data based on keywords
 */
public class KeywordTag extends Tag {

	private List<String> keyWords = new ArrayList<String>();

	public KeywordTag(String... keyWords) {
		for (String keyWord: keyWords) {
			this.keyWords.add(keyWord);
		}
	}

	public void collectData(List<MyData> dataList) {
		for (MyData data: dataList) {
			List<String> contentList = data.getContents();
			int numberOfValid[] = new int[contentList.size()];
			for (String keyWord: keyWords) {
				int index = 0;
				String possibleKeywords[] = keyWord.split("/");
				for (String content: contentList) {
					for (String k: possibleKeywords) {
						if (content.toLowerCase().contains(k.toLowerCase())) {
							numberOfValid[index]++;
							break;
						}
					}
					index++;
				}
			}
			for (int i = 0; i < contentList.size(); i++) {
				if (numberOfValid[i] == keyWords.size()) {
					relevantData.add(contentList.get(i));
				}
			}
		}
	}

}
