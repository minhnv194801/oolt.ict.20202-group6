package hust.soict.globalict.main.tag;

public interface Mergeable<E> {

	public void merge(E... objectsToMerged);

}
