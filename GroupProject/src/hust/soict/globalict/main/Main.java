package hust.soict.globalict.main;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.main.data.MyData;
import hust.soict.globalict.main.initializer.Initializer;
import hust.soict.globalict.main.tag.DateTag;
import hust.soict.globalict.main.tag.KeywordTag;
import hust.soict.globalict.main.tag.Tag;

public class Main {

	public static void main(String args[]) {
		List<MyData> myData = new ArrayList<MyData>();
		List<Tag> tagList = new ArrayList<Tag>();
		Scanner sc = new Scanner(System.in);
		String buffer;
		
		System.out.println("This system may take a while to start");
		Initializer.initialData(myData);
		System.out.println("Welcome");

		System.out.print("Nhập ngày/tháng/năm (d/m/y): ");
		buffer = sc.nextLine();
		buffer = buffer.trim();
		if (!buffer.equals("")) {
			Tag newTag = new DateTag(buffer);
			newTag.collectData(myData);
			tagList.add(newTag);
		}

		System.out.print("Nhập phiên trong ngày (sáng/chiều/tối): ");
		buffer = sc.nextLine();
		buffer = buffer.trim();
		if (!buffer.equals("")) {
			if (buffer.toLowerCase().equals("sáng") || buffer.toLowerCase().equals("chiều") || buffer.toLowerCase().equals("tối")) {
				Tag newTag = new KeywordTag("phiên " + buffer);
				newTag.collectData(myData);
				tagList.add(newTag);
			}
			else {
				System.out.println("Invalid format");
			}
		}

		System.out.print("Nhập sàn chứng khoán: ");
		buffer = sc.nextLine();
		buffer = buffer.trim();
		if (!buffer.equals("")) {
			Tag newTag = new KeywordTag(buffer);
			newTag.collectData(myData);
			tagList.add(newTag);
		}

		System.out.print("Nhập mã chứng khoán: ");
		buffer = sc.nextLine();
		buffer = buffer.trim();
		if (!buffer.equals("")) {
			Tag newTag = new KeywordTag(buffer);
			newTag.collectData(myData);
			tagList.add(newTag);
		}

		System.out.println("Bạn có quan tâm đến Vn-index? (Yes/No)");
		buffer = sc.nextLine();
		buffer = buffer.trim();
		if (!buffer.equals("")) {
			if (buffer.equals("yes")) {
				Tag newTag = new KeywordTag("vn-index");
				newTag.collectData(myData);
				tagList.add(newTag);
			}
		}
		
		if (!tagList.isEmpty()) {
			Tag finalTag = tagList.get(0);
			tagList.remove(0);
			for (Tag aTag: tagList) {
				finalTag.merge(aTag);
			}
			finalTag.print();
		}

		System.out.println("Finish");
		sc.close();
	}
}
