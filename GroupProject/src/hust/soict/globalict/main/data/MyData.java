package hust.soict.globalict.main.data;
import java.util.ArrayList;
import java.util.List;

/**
 * MyData is used to stored the data extracted from articles
 * The information extracted from articles csv files will have the format of:
 * "rownumber","link","title","description","date","author","[contents]"
 * This class will process that string format into many String for more convenient use
 * Note that the contents part is very huge so it will be further broken down into many smaller string
 */
public class MyData {

	private static int nbData = 0;

	private int id;
	private String link, title, description, author;
	private int day, month, year, hour;
	List<String> contents = new ArrayList<String>();

	public MyData(String articleData) {

		String tokens[] = articleData.split("\",\"");
		String date, content;

		if (tokens.length == 7) {
			nbData++;
			id = nbData;
			link = tokens[1];
			title = tokens[2];
			description = tokens[3];
			date = tokens[4];
			processTime(date);
			author = tokens[5];
			content = tokens[6];
			processContent(content);
		}
	}

	private void processTime(String date) {
		if (!date.equalsIgnoreCase("error")) {
			String tokens[] = date.split(", ");
			if (tokens.length == 3) {
				String dmy[] = tokens[1].split("/");
				day = Integer.parseInt(dmy[0]);
				month = Integer.parseInt(dmy[1]);
				year = Integer.parseInt(dmy[2]);

				String hm[] = tokens[2].split(":");
				hour = Integer.parseInt(hm[0]);
			}
			else if (tokens.length == 2) {
				String tmp[] = tokens[1].split(" ");

				String dmy[] = tmp[0].split("/");
				day = Integer.parseInt(dmy[0]);
				month = Integer.parseInt(dmy[1]);
				year = Integer.parseInt(dmy[2]);

				String hm[] = tmp[1].split(":");
				hour = Integer.parseInt(hm[0]);
			}
			else if (tokens.length == 1) {
				boolean isEvening = false;
				String tmp[] = tokens[0].split(" - ");

				String dmy[] = tmp[0].split("-");
				day = Integer.parseInt(dmy[0]);
				month = Integer.parseInt(dmy[1]);
				year = Integer.parseInt(dmy[2]);

				if (tmp[1].substring(tmp[1].length() - 2, tmp[1].length() - 1).equalsIgnoreCase("pm")) {
					isEvening = true;
				}
				String hm[] = tmp[1].split(":");
				hour = Integer.parseInt(hm[0]);
				if (isEvening) {
					hour += 12;
				}
			}
		}
	}

	private void processContent(String content) {
		if (content.length() > 3) {
			String tokens[] = content.substring(2, content.length() - 3).split("', '");
			for (String token: tokens) {
				String lines[] = token.split("\\. ");
				for (String aLine: lines) {
					aLine = aLine.trim().replaceAll(" +", " ");
					aLine = aLine.replace("\\n", "");
					aLine = aLine.replace("\\xa0", " ");
					contents.add(aLine);
				}
			}
		}
	}

	public int getId() {
		return this.id;
	}
	public String getLink() {
		return this.link;
	}
	public String getTitle() {
		return this.title;
	}
	public String getDescription() {
		return this.description;
	}
	public String getAuthor() {
		return this.author;
	}
	public int getDay() {
		return this.day;
	}
	public int getMonth() {
		return this.month;
	}
	public int getYear() {
		return this.year;
	}
	public int getHour() {
		return this.hour;
	}
	public List<String> getContents() {
		return this.contents;
	}
}
